FROM  mdillon/postgis:9.6

ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD changeMe

ADD https://openbiomaps.org/projects/checkitout/template.php?sql&biomaps /docker-entrypoint-initdb.d/01-biomaps.sql
ADD https://openbiomaps.org/projects/checkitout/template.php?sql&gisdata /docker-entrypoint-initdb.d/11-gisdata.sql

RUN sed -i 's/--CREATE DATABASE/CREATE DATABASE/'  /docker-entrypoint-initdb.d/01-biomaps.sql
RUN sed -i 's/--\\c biomaps/\\c biomaps/'  /docker-entrypoint-initdb.d/01-biomaps.sql

RUN sed -i 's/--CREATE DATABASE/CREATE DATABASE/'  /docker-entrypoint-initdb.d/11-gisdata.sql
RUN sed -i 's/--\\c gisdata/\\c gisdata/'  /docker-entrypoint-initdb.d/11-gisdata.sql
RUN sed -i 's/CREATE ROLE biomapsadmin/--CREATE ROLE biomapsadmin/'  /docker-entrypoint-initdb.d/11-gisdata.sql

RUN chmod 444 /docker-entrypoint-initdb.d/*.sql
